/*相关文章HTML*/
function GetRelatedList(newsid, count, noImg){
    if (tag_jsonp.length > 1) {
		if(count == -1) count = 3;
		var className = "related_post";
		if(tag_jsonp[0].img !== undefined){
		    className = "related_post_new";
		}
	    var html = ("<div class=\"related_post_common " + className + "\"><h2 class=\"related_title_h\">相关文章</h2><ul class=\"list_1\">");
	    for (var i = 0; i < tag_jsonp.length && i<count; i++) {
            if(parseInt(tag_jsonp[i].newsid) == newsid)
                continue;

//            if(tag_jsonp[0].img !== undefined && noImg == 0){
//                //有缩略图
//                html += ("<li class='material-ripple' onclick='javascript:ProxyClickPicture.openRelatedPost(" + tag_jsonp[i].newsid + ");'>"
//                            + "<img src='" + tag_jsonp[i].img + "'>"
//                            + "<div class='related_meta'>"
//                            + "<span class='related_title'>" + tag_jsonp[i].newstitle + "</span>"
//                            + "<span class='related_date'>" + tag_jsonp[i].postdate +  "</span>"
//                            + "</div></li>");
//            }else{
//                //无缩略图
//                html += ("<li class='material-ripple' onclick='javascript:ProxyClickPicture.openRelatedPost(" + tag_jsonp[i].newsid + ");'>"
//                            + "<span class='related_title'>" + tag_jsonp[i].newstitle + "</span>"
//                            + "<span class='date'>" + tag_jsonp[i].postdate +  "</span>"
//                            + "</li>");
//            }

            if (tag_jsonp[i].img == undefined) {
                tag_jsonp[i].img = "null";
            }

            if (noImg) {
                tag_jsonp[i].img = "null";
            }

            var hasImg = tag_jsonp[0].img !== "null" && noImg == 0;
            html += ("<li class='material-ripple' onclick='javascript:ProxyClickPicture.openRelatedPost(" + tag_jsonp[i].newsid + ");'>"
                            + "<img onerror='loadErrorImage();' data-errorImg='file:///android_asset/newsInfo/news_thumb_placeholder.png' src='" + tag_jsonp[i].img + "' style='" + (hasImg ? "" : "display:none;") + "'>"
                            + "<div class='related_meta' style='" + (hasImg ? "" : "margin-left:0px;") + "'>"
                            + "<span class='related_title'>" + tag_jsonp[i].newstitle + "</span>"
                            + "<span class='related_date'>" + tag_jsonp[i].postdate +  "</span>"
                            + "</div></li>");
	    }
	    html += ("</ul></div>");
	    document.getElementById("related").innerHTML = html;
	}
}