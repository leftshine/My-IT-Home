package cn.leftshine.myithome.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Administrator on 2018/9/18.
 */

public class DateTool {
    public static Date isoStrParseToDate(String isoDateStr) {
        Date result = null;
        try {
            String pattern = "yyyy-MM-dd'T'HH:mm:ssZ";
            SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.CHINA);
            isoDateStr = sdf.format(new Date(isoDateStr));
            result = sdf.parse(isoDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }
}
