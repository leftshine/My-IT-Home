package cn.leftshine.myithome.utils;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.leftshine.myithome.beans.NewsItem;
import cn.leftshine.myithome.ui.fragment.NewsListFragment;

import static cn.leftshine.myithome.ui.fragment.NewsListFragment.MSG_XML_PARSE_COMPLETE;

/**
 * Created by Administrator on 2018/9/12.
 */

public class RssParseTool {

    public RssParseTool() {
    }

    public List<NewsItem> parseXMLWithPull(String xmlData, Handler handler) {
        List<NewsItem> items = new ArrayList<>();
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser xmlPullParser = factory.newPullParser();
            xmlPullParser.setInput(new StringReader(xmlData));
            int eventType  = xmlPullParser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT){
                String nodeName = xmlPullParser.getName();
                NewsItem newsItem = new NewsItem();
                switch (eventType){
                    //开始解析某个节点
                    case XmlPullParser.START_TAG:{
                        if(xmlPullParser.getDepth()==4){
                            /*if("title".equals(nodeName)){
                                newsItem.title= xmlPullParser.nextText();
                                Log.i("pcj", "parseXMLWithPull: depth="+ xmlPullParser.getDepth()+"title="+newsItem.title);
                            }else if("link".equals(nodeName)){
                                newsItem.link=xmlPullParser.nextText();
                            }else if("guid".equals(nodeName)){
                                newsItem.guid=xmlPullParser.nextText();
                            }else if("description".equals(nodeName)){
                                newsItem.description = xmlPullParser.nextText();
                            }else if("pubDate".equals(nodeName)){
                                newsItem.pubDate = xmlPullParser.nextText();
                            }*/
                            if("title".equals(nodeName)){
                                newsItem.title= xmlPullParser.nextText();
                                xmlPullParser.nextTag();
                                newsItem.link=xmlPullParser.nextText();
                                xmlPullParser.nextTag();
                                newsItem.guid=xmlPullParser.nextText();
                                xmlPullParser.nextTag();
                                newsItem.description=xmlPullParser.nextText();
                                xmlPullParser.nextTag();
                                newsItem.pubDate=xmlPullParser.nextText();
                                items.add(newsItem);
                                Log.i("pcj", "parseXMLWithPull: depth="+ xmlPullParser.getDepth()+"title="+newsItem.title+"\nlink="+newsItem.link);
                            }

                        }
                        break;
                    }
                    //完成解析某个节点
                    case XmlPullParser.END_TAG:{
                        /*if("item".equals(nodeName)){
                            items.add(newsItem);
                        }else if("channel".equals(nodeName)){
                            Message msg = Message.obtain();
                            msg.what=MSG_XML_PARSE_COMPLETE;
                            msg.obj= items;
                            handler.sendMessage(msg);
                        }*/
                        if("channel".equals(nodeName)) {
                            Message msg = Message.obtain();
                            msg.what = MSG_XML_PARSE_COMPLETE;
                            msg.obj = items;
                            handler.sendMessage(msg);
                        }
                        break;
                    }
                    default:
                        break;
                }
                eventType = xmlPullParser.next();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return items;
    }

}
