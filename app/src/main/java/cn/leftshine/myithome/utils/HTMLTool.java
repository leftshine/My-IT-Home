package cn.leftshine.myithome.utils;

/**
 * Created by Administrator on 2018/9/21.
 */

public class HTMLTool {
    private   static String getCssStr(String ... strings){
        String cssStr="";
        for (int i=0;i<strings.length;i++){
            //"<link rel=\"stylesheet\" type=\"text/css\" href=\"css/LapinCardCss.css\" /> "
            cssStr += "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/"+strings[i]+".css\" /> ";
        }
        return cssStr;
    }
    private  static String getJsStr(String ... strings){
        String jsStr="";
        for (int i=0;i<strings.length;i++){
            //"<script src="file:///android_asset/jquery.min.js"></script>"
            jsStr += "<script src=\"js/"+strings[i]+".js\"></script> ";
        }
        return jsStr;
    }

    public static String createHtmlDate(String title, String content, String link) {
        String newsID = link.substring(link.length()-10,link.length()-4);
        String newsTag = "0"+newsID.substring(0,3);
        /*String cssSrc = "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/LapinCardCss.css\" /> " +
                "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/news.css\" />" +
                "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/normalize.css\" />" +
                "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/prettify.css\" />";*/
        String cssData = getCssStr("LapinCardCss","news","normalize","LinkCardCss","ripple.min","news_detail")
                +"<link id=\"myPrettify\" rel=\"stylesheet\" href=\"css/prettify.css\" type=\"text/css\">";
        String jsData = getJsStr("jquery.lazyload","jquery.min","prettify")
                +"<script>function copyToAppClickboard(copyText){copyTool.notifyAppCopyText(copyText, \"复制成功\");}</script>";
        String htmlData ="<html lang=\"zh-CN\">" +
                "<head>" +
                "<meta charset=\"utf-8\"><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\"><meta name=\"format-detection\" content=\"telephone=no\"><meta http-equiv=\"Cache-control\" content=\"public\"><meta name=\"viewport\" content=\"width=device-width,initial-scale=1,user-scalable=no,minimum-scale=1.0,maximum-scale=1.0\">"
                +cssData+jsData
                +"<title>"+title+"</title>" +
                "</head>" +
                "<body class=\"ithome-android \">" +
                "<div class=\"content\" style=\"margin-top:17px;\">" + content +"</div>" +
                "<div class=\"zebian_container\"><span id=\"zebian\">责任编辑：骑士</span><div id=\"errorPick\" class=\"material-button material-ripple\" onclick=\"javascript:ProxyClickPicture.errorPick();\">我要纠错</div></div>" +
                "<div class=\"userGrade\" id=\"userGrade\"><div class=\"totalScore\" style=\"display:none\"><span id=\"totalScoreText\">文章价值: 3.7 分</span><div id=\"starDiv\"><div id=\"starDefaultBack\"></div><div id=\"starBackground\" style=\"width: 74px;\"></div><div id=\"star\"></div></div><span id=\"userAmount\">21人打分</span></div><span class=\"tipsspan\" style=\"display: block;\">打分后显示文章质量得分，当前21人打分</span><div class=\"gradeButtons\"><div id=\"valueLess\" class=\"gradeButton material-button material-ripple\" onclick=\"javascript:ProxyClickPicture.gradeLess();\"><span class=\"value\" id=\"valueLessText\">无价值</span><span class=\"gradeAmount\" id=\"valueLessAmount\">6</span></div><div id=\"valueNormal\" class=\"gradeButton material-button material-ripple\" onclick=\"javascript:ProxyClickPicture.gradeNormal();\"><span class=\"value\" id=\"valueNormalText\">还可以</span><span class=\"gradeAmount\" id=\"valueNormalAmount\">2</span></div><div id=\"valueMost\" class=\"gradeButton material-button material-ripple\" onclick=\"javascript:ProxyClickPicture.gradeMost();\"><span class=\"value\" id=\"valueMostText\">有价值</span><span class=\"gradeAmount\" id=\"valueMostAmount\">13</span></div></div></div>" +
                "<script src=\"http://api.ithome.com/json/tags/"+newsTag+"/"+newsID+".json\" type=\"text/javascript\"></script>" +
                getJsStr("apprelatedv2")+
                //"<script src=\"file:///android_asset/apprelatedv2.js\"></script>" +
                "<div id=\"related\" style=\"margin-top:23.799999999999997px;\"><div class=\"related_post_common related_post_new\"><h2 class=\"related_title_h\">相关文章</h2><ul class=\"list_1\"><li class=\"material-ripple\" onclick=\"javascript:ProxyClickPicture.openRelatedPost(384559);\"><img onerror=\"loadErrorImage();\" data-errorimg=\"file:///android_asset/newsInfo/news_thumb_placeholder.png\" src=\"http://img.ithome.com/newsuploadfiles/thumbnail/2018/9/384559_240.jpg\" style=\"\"><div class=\"related_meta\" style=\"\"><span class=\"related_title\">iPhone那么贵的10个秘密：让苹果继续飞一会儿</span><span class=\"related_date\">2018.09.21</span></div></li><li class=\"material-ripple\" onclick=\"javascript:ProxyClickPicture.openRelatedPost(384525);\"><img onerror=\"loadErrorImage();\" data-errorimg=\"file:///android_asset/newsInfo/news_thumb_placeholder.png\" src=\"http://img.ithome.com/newsuploadfiles/thumbnail/2018/9/384525_240.jpg\" style=\"\"><div class=\"related_meta\" style=\"\"><span class=\"related_title\">京东为iPhone XS推出“换修无忧”服务：最低每月39元</span><span class=\"related_date\">2018.09.21</span></div></li><li class=\"material-ripple\" onclick=\"javascript:ProxyClickPicture.openRelatedPost(384509);\"><img onerror=\"loadErrorImage();\" data-errorimg=\"file:///android_asset/newsInfo/news_thumb_placeholder.png\" src=\"http://img.ithome.com/newsuploadfiles/thumbnail/2018/9/384509_240.jpg\" style=\"\"><div class=\"related_meta\" style=\"\"><span class=\"related_title\">苹果iPhone XS的“入门指南”显示，AirPower尚存生机</span><span class=\"related_date\">2018.09.21</span></div></li></ul></div></div>" +
                "<script> GetRelatedList(384566,3,0);</script>" +
                "<div class=\"hotComment_placeholder\"><div id=\"newshotcomment\" style=\"margin-top:20.4px;\"><h3 class=\"icon2 related_title_h\">最热评论</h3><ul class=\"list\"><li class=\"entry\" onclick=\"javascript:ProxyClickPicture.openComment();\"><div class=\"info rmp\"><img class=\"headerimage\" onerror=\"loadErrorImage();\" data-errorimg=\"file:///android_asset/avatar_default_rect.png\" src=\"http://avatar.ithome.com/avatars/000/08/03/46_60.jpg\"><span class=\"user_grade\">Lv.24</span><div class=\"allnoimage\"><div class=\"nameanddev\"><strong class=\"nick \"><a>sdo小zheng_2</a></strong><span class=\"hot_comment_tail hot_comment_tail_iphonex\" onclick=\"javascript:event.cancelBubble=true;ProxyClickPicture.clickTail(this.innerHTML);\">iPhone XS Max 深空灰</span></div><span class=\"posandtime\">IT之家上海网友&nbsp;2018-09-21 11:39</span></div><span class=\"dingcount\">171顶</span></div><div class=\"comment_body\"><p>看看尾巴</p></div></li><li class=\"entry\" onclick=\"javascript:ProxyClickPicture.openComment();\"><div class=\"info rmp\"><img class=\"headerimage\" onerror=\"loadErrorImage();\" data-errorimg=\"file:///android_asset/avatar_default_rect.png\" src=\"http://avatar.ithome.com/avatars/001/60/76/87_60.jpg\"><span class=\"user_grade\">Lv.29</span><div class=\"allnoimage\"><div class=\"nameanddev\"><strong class=\"nick \"><a>谷歌党政办</a></strong></div><span class=\"posandtime\">IT之家山东潍坊网友&nbsp;2018-09-21 11:43</span></div><span class=\"dingcount\">111顶</span></div><div class=\"comment_body\"><p>今天的热评让我感到贫困...</p></div></li><li class=\"entry\" onclick=\"javascript:ProxyClickPicture.openComment();\"><div class=\"info rmp\"><img class=\"headerimage\" onerror=\"loadErrorImage();\" data-errorimg=\"file:///android_asset/avatar_default_rect.png\" src=\"http://avatar.ithome.com/avatars/000/94/66/77_60.jpg\"><span class=\"user_grade\">Lv.23</span><div class=\"allnoimage\"><div class=\"nameanddev\"><strong class=\"nick \"><a>Grim丿reaper</a></strong><span class=\"hot_comment_tail hot_comment_tail_iphonex\" onclick=\"javascript:event.cancelBubble=true;ProxyClickPicture.clickTail(this.innerHTML);\">iPhone XS Max 深空灰</span></div><span class=\"posandtime\">IT之家浙江宁波网友&nbsp;2018-09-21 11:36</span></div><span class=\"dingcount\">84顶</span></div><div class=\"comment_body\"><p>有点东西的啊</p></div></li></ul><div class=\"more_comm\"><a id=\"pagecomment\" href=\"javascript:ProxyClickPicture.openComment();\">查看更多评论</a></div></div></div>" +
                getJsStr("info")+
                //"<script src=\"file:///android_asset/info.js\"></script>" +
                "<script>$(function(){$(\"img.lazy\").lazyload({effect:\"fadeIn\", threshold:100});$(\"pre\").addClass(\"prettyprint\");prettyPrint();ProxyClickPicture.autoScroll()});</script>" +
                getJsStr("ripple.min")+
                //"<script src=\"file:///android_asset/newsInfo/ripple.min.js\"></script>" +
                "</body>" +
                "</html>";

        return htmlData;
    }
}
