package cn.leftshine.myithome.beans;

import org.w3c.dom.CDATASection;

import java.util.Date;

/**
 * Created by Administrator on 2018/9/12.
 */
/*
<item>
    <title>小米手环3 NFC限量纪念版公布：全球666支还买不到，腕带都限量</title>
    <link>https://www.ithome.com/html/next/382692.htm</link>
    <guid>https://www.ithome.com/html/next/382692.htm</guid>
    <description>
        <![CDATA[
            <p>IT之家9月12日消息&nbsp;昨天，小米官方宣布，小米手环全球累计出货量突破5000万支，小米手环3 NFC版本也将于9月19日全渠道开售，售价199元。</p><p>今天，小米更是通过米家微博宣布了小米手环3 NFC限量纪念版，全球限量666支。小米表示，这一款小米手环3并不会单独售卖，而是以参与活动免费赠送的方式送给用户。</p><p><img src="http://img.ithome.com/newsuploadfiles/2018/9/20180912152617_2250.jpg@wm_1,k_aW1nL3FkLnBuZw==,y_20,o_100,x_20,g_3" w="600" h="399"/></p><p>作为一款限量纪念版机型，这款小米手环3将拥有独一无二的枫叶红腕带，不再续产；环扣上带有唯一镭雕图案，全球限量编号；使用专属定制包装盒。</p><p><img src="http://img.ithome.com/newsuploadfiles/2018/9/20180912152624_4303.jpg@wm_1,k_aW1nL3FkLnBuZw==,y_20,o_100,x_20,g_3" w="600" h="400"/></p><p>小米手环3 NFC限量纪念版手环获取的官方渠道仅有参与小米活动，中奖者将由小米送出这一限量版手环。</p><p><img src="http://img.ithome.com/newsuploadfiles/2018/9/20180912_152654_838.jpg@wm_1,k_aW1nL3FkLnBuZw==,y_20,o_100,x_20,g_3" w="598" h="456"/></p>
        ]]>
    </description>
    <pubDate>Wed, 12 Sep 2018 07:28:28 GMT</pubDate>
</item>
*/
public class NewsItem {
    public String title;
    public String link;
    public String guid;
    public String description;
    public String pubDate;
    public String comment;
    public String imgSrc;
}
