package cn.leftshine.myithome.ui.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationMenu;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.tobiasrohloff.view.NestedScrollWebView;

import cn.leftshine.myithome.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LapinFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LapinFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LapinFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public NestedScrollWebView webView;
    //private NestedScrollView nestedScrollView;
    private LinearLayout ll_main_bottom;
    private boolean isBottomShow = true;

    public LapinFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LapinFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LapinFragment newInstance(String param1, String param2) {
        LapinFragment fragment = new LapinFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lapin, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        webView = getView().findViewById(R.id.webview_lapin);
        if(webView!=null){
            webView.loadUrl("https://m.lapin365.com/");
        }
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if(url.startsWith("tbopen"))
                    return true;
                else
                    view.loadUrl(url);
                return true;
            }

        });
        webView.getSettings().setJavaScriptEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            webView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    ll_main_bottom=getActivity().findViewById(R.id.ll_main_bottom);

                    //上滑 并且 正在显示底部栏
                    if (scrollY - oldScrollY > 0 && isBottomShow) {
                        isBottomShow = false;
                        //将Y属性变为底部栏高度  (相当于隐藏了)
                        ll_main_bottom.animate().translationY(ll_main_bottom.getHeight());
                    } else if (scrollY - oldScrollY < 0 && !isBottomShow) {
                        isBottomShow = true;
                        ll_main_bottom.animate().translationY(0);
                    }
                }
            });
        }
        /*nestedScrollView = getView().findViewById(R.id.nestView_lapin);
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int
                    oldScrollX, int oldScrollY) {
                bottomNavigationView=getActivity().findViewById(R.id.bottom_navigation);

                //上滑 并且 正在显示底部栏
                if (scrollY - oldScrollY > 0 && isBottomShow) {
                    isBottomShow = false;
                    //将Y属性变为底部栏高度  (相当于隐藏了)
                    bottomNavigationView.animate().translationY(bottomNavigationView.getHeight());
                } else if (scrollY - oldScrollY < 0 && !isBottomShow) {
                    isBottomShow = true;
                    bottomNavigationView.animate().translationY(0);
                }

            }

        });*/
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
