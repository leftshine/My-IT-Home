package cn.leftshine.myithome.ui.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cn.leftshine.myithome.R;
import cn.leftshine.myithome.adapter.NewsViewPageAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NewsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NewsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private NestedScrollView nestedScrollView;
    private LinearLayout ll_main_bottom;
    private boolean isBottomShow =true;
    private TabLayout tabs_news;
    private ViewPager viewPager_news;


    public NewsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NewsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NewsFragment newInstance(String param1, String param2) {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        Toolbar toolbar = view.findViewById(R.id.toolbar_news);
        tabs_news  = view.findViewById(R.id.tabs_news);
        viewPager_news = view .findViewById(R.id.viewPager_news);

        toolbar.setTitle("");
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArrayList<String> titleDatas=new ArrayList<>();
        titleDatas.add("最新");
        titleDatas.add("排行榜");
        titleDatas.add("安卓");
        titleDatas.add("原创");
        titleDatas.add("上热评");
        titleDatas.add("评测室");
        titleDatas.add("发布会");
        titleDatas.add("阳台");
        titleDatas.add("手机");
        titleDatas.add("数码");
        titleDatas.add("极客学院");
        titleDatas.add("VR");
        titleDatas.add("智能汽车");
        titleDatas.add("电脑");
        ArrayList<Fragment> fragmentList = new ArrayList<Fragment>();
        for(int i=0;i<titleDatas.size();i++) {
            NewsListFragment newsListFragment = new NewsListFragment();
            Bundle bundle = new Bundle();
            bundle.putString("title",titleDatas.get(i));
            newsListFragment.setArguments(bundle);
            fragmentList.add(newsListFragment);
        }
        NewsViewPageAdapter myViewPageAdapter = new NewsViewPageAdapter(getChildFragmentManager(), titleDatas, fragmentList);
        viewPager_news.setAdapter(myViewPageAdapter);
        tabs_news.setupWithViewPager(viewPager_news);
        tabs_news.setTabsFromPagerAdapter(myViewPageAdapter);
        /*nestedScrollView = getView().findViewById(R.id.nestView_news);
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int
                    oldScrollX, int oldScrollY) {
                ll_main_bottom=getActivity().findViewById(R.id.ll_main_bottom);

                //上滑 并且 正在显示底部栏
                if (scrollY - oldScrollY > 0 && isBottomShow) {
                    isBottomShow = false;
                    //将Y属性变为底部栏高度  (相当于隐藏了)
                    ll_main_bottom.animate().translationY(ll_main_bottom.getHeight());
                } else if (scrollY - oldScrollY < 0 && !isBottomShow) {
                    isBottomShow = true;
                    ll_main_bottom.animate().translationY(0);
                }

            }

        });*/
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_news, menu);
        if (menu != null) {
            if (menu.getClass() == MenuBuilder.class) {
                try {
                    Method m = menu.getClass().getDeclaredMethod("setOptionalIconsVisible", Boolean.TYPE);
                    m.setAccessible(true);
                    m.invoke(menu, true);
                } catch (Exception e) {

                }
            }
        }
        final MenuItem item = menu.findItem(R.id.action_news_calender);
        TextView tv_toolbar_today = item.getActionView().findViewById(R.id.tv_toolbar_today);
        SimpleDateFormat sdf = new SimpleDateFormat("dd");
        tv_toolbar_today.setText(sdf.format(new Date()));
        super.onCreateOptionsMenu(menu, inflater);

    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
